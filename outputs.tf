output "instance_a_ip" {
    value = "${google_compute_instance.instance_a.network_interface.0.network_ip}"
}