variable "name" {}

variable "image" {}

variable "subnetwork" {}

variable "labels" {
  type = "map"
  default = {
      "key1" = "value00"
  }
}
