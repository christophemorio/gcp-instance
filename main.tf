resource "google_compute_instance" "instance_a" {
  name         = "${var.name}"
  machine_type = "g1-small"
  zone         = "europe-west1-b"

  boot_disk {
    initialize_params {
      image = "${var.image}"
    }
  }

  network_interface {
    subnetwork    = "${var.subnetwork}"
    access_config = {}
  }

  labels {
      key1 = "${var.labels["key1"]}"
  }


  tags = ["fw-global-external-google", "fw-global-internal-admin"]
}

